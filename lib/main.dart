import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_tres_astronautas/locator.dart';
import 'package:prueba_tres_astronautas/routes.dart';

void main() {
  setUp();
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => ScreenUtilInit(
        designSize: const Size(375, 812),
        builder: (context, widget) => const MaterialApp(
          debugShowCheckedModeBanner: false,
          onGenerateRoute: Routes.onGenerate,
        ),
      );
}
