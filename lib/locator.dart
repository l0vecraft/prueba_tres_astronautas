import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:prueba_tres_astronautas/core/api/network/restaurant_api.dart';
import 'package:prueba_tres_astronautas/core/api/repository/restaurant_repository.dart';

var l = GetIt.I;

final _dio = Dio();

void setUp() {
  l.registerLazySingleton<RestaurantRepository>(() => RestaurantApi(dio: _dio));
}
