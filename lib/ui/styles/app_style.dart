import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Appstyle {
  static TextStyle title1 =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 30.sp);

  static TextStyle title2 =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 25.sp);

  static TextStyle title3 =
      TextStyle(fontSize: 18.sp, fontWeight: FontWeight.w700);
  static TextStyle subtitle1 = TextStyle(
      color: Colors.grey.shade600,
      fontWeight: FontWeight.w500,
      fontSize: 16.sp);

  //---------------- Container decorations --------------------------
  static BorderRadius cardBorderRadius1 = BorderRadius.circular(10);
  static BorderRadius borderRadiusOnlyTop = const BorderRadius.only(
      topLeft: Radius.circular(30), topRight: Radius.circular(30));

  static OutlineInputBorder inputBorderRadius = OutlineInputBorder(
      borderSide: const BorderSide(color: Colors.orange),
      borderRadius: Appstyle.cardBorderRadius1);
}
