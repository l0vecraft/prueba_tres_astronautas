import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_tres_astronautas/core/const/const.dart';
import 'package:prueba_tres_astronautas/core/models/data_model.dart';
import 'package:prueba_tres_astronautas/ui/screens/detail/widget/text_with_icon.dart';
import 'package:prueba_tres_astronautas/ui/screens/detail/widget/title_wit_subtitle_score.dart';
import 'package:prueba_tres_astronautas/ui/styles/app_style.dart';

class DetailScreen extends ConsumerWidget {
  final DataModel restaurant;
  const DetailScreen({super.key, required this.restaurant});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Container(
            alignment: Alignment.topCenter,
            child: Image.network(
              restaurant.images!.original!.url!,
              fit: BoxFit.fill,
              height: double.parse(restaurant.images!.original!.height!) <= 480
                  ? 370
                  : null,
            ),
          ),
          Container(
            height: 500.h,
            padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 15.w),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: Appstyle.borderRadiusOnlyTop),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TitleWithSubtitleAndScore(
                  title: restaurant.user!.username!,
                  subtitle: restaurant.title!,
                ),
                Text(
                  template_text,
                  style: TextStyle(fontSize: 18.sp),
                ),
                SizedBox(height: 8.h),
                const TextWithIcon(
                    text: 'Default location of the restaurant',
                    icon: Icons.location_on_outlined)
              ],
            ),
          ),
        ],
      ),
    );
  }
}
