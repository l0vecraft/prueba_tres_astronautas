import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_tres_astronautas/ui/styles/app_style.dart';

class TitleWithSubtitleAndScore extends StatelessWidget {
  const TitleWithSubtitleAndScore(
      {Key? key, required this.title, required this.subtitle})
      : super(key: key);

  final String title;
  final String subtitle;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 8.h),
                child: Text(
                  title,
                  style: Appstyle.title3,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10.h),
                child: Text(
                  subtitle,
                  style: Appstyle.subtitle1,
                ),
              )
            ],
          ),
        ),
        const Icon(
          Icons.star_rate_sharp,
          color: Colors.orange,
        ),
        const Text('4,5'),
        IconButton(
            onPressed: () {}, icon: const Icon(Icons.favorite_border_rounded))
      ],
    );
  }
}
