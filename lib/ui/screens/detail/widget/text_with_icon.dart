import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TextWithIcon extends StatelessWidget {
  final IconData icon;
  final String text;
  const TextWithIcon({
    Key? key,
    required this.icon,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          icon,
          size: 24.sp,
        ),
        SizedBox(width: 4.w),
        Text(
          text,
          style: TextStyle(fontSize: 16.sp, color: Colors.grey.shade600),
        )
      ],
    );
  }
}
