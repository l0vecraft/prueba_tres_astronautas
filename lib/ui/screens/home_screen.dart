import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:line_icons/line_icons.dart';
import 'package:prueba_tres_astronautas/core/providers/restaurant_provider.dart';
import 'package:prueba_tres_astronautas/ui/screens/date_screen.dart';
import 'package:prueba_tres_astronautas/ui/screens/favorite/favorite_screen.dart';
import 'package:prueba_tres_astronautas/ui/screens/restaurant/restaurant_screen.dart';
import 'package:prueba_tres_astronautas/ui/screens/search_screen.dart';
import 'package:prueba_tres_astronautas/ui/widget/circular_icon_buttom.dart';
import 'package:prueba_tres_astronautas/ui/widget/custom_loading.dart';

class HomeScreen extends ConsumerStatefulWidget {
  const HomeScreen({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _HomeScreenState();
}

class _HomeScreenState extends ConsumerState<HomeScreen> {
  var _currentIndex = 0;
  late PageController _controller;

  @override
  void initState() {
    super.initState();
    _controller = PageController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          decoration: const BoxDecoration(),
          child: Image.asset(
            'images/NASA_logo.png',
            height: 65.h,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        actions: [
          CircularIconButton(icon: LineIcons.cog, onTap: () {}),
          CircularIconButton(icon: LineIcons.bell, onTap: () {})
        ],
      ),
      body: PageView(
        controller: _controller,
        onPageChanged: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        children: const [
          RestaurantScreen(),
          DateScreen(),
          SearchScreen(),
          FavoriteScreen()
        ],
      ),
      bottomNavigationBar: BottomNavyBar(
          selectedIndex: _currentIndex,
          animationDuration: const Duration(milliseconds: 300),
          items: [
            BottomNavyBarItem(
                activeColor: Colors.black,
                inactiveColor: Colors.grey,
                icon: const Icon(LineIcons.home),
                title: const Text('Home')),
            BottomNavyBarItem(
                activeColor: Colors.black,
                inactiveColor: Colors.grey,
                icon: const Icon(LineIcons.calendarWithWeekFocus),
                title: const Text('Date')),
            BottomNavyBarItem(
                activeColor: Colors.black,
                inactiveColor: Colors.grey,
                icon: const Icon(LineIcons.search),
                title: const Text('Search')),
            BottomNavyBarItem(
                activeColor: Colors.black,
                inactiveColor: Colors.grey,
                icon: const Icon(Icons.favorite_outline_rounded),
                title: const Text('Favorites'))
          ],
          onItemSelected: (int value) {
            setState(() => _currentIndex = value);
            _controller.jumpToPage(value);
          }),
    );
  }
}
