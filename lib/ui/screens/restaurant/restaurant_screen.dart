import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_tres_astronautas/core/models/category_model.dart';
import 'package:prueba_tres_astronautas/core/providers/restaurant_provider.dart';
import 'package:prueba_tres_astronautas/ui/screens/restaurant/widget/card_restaurant.dart';
import 'package:prueba_tres_astronautas/ui/widget/category_pill.dart';
import 'package:prueba_tres_astronautas/ui/widget/custom_loading.dart';
import 'package:prueba_tres_astronautas/ui/widget/custom_title.dart';

class RestaurantScreen extends ConsumerStatefulWidget {
  const RestaurantScreen({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _RestaurantScreenState();
}

class _RestaurantScreenState extends ConsumerState<RestaurantScreen> {
  @override
  Widget build(BuildContext context) {
    final restaurants = ref.watch(restaurantInitData);
    return restaurants.when(
        data: (data) => Container(
            padding: EdgeInsets.symmetric(horizontal: 10.w),
            child: CustomScrollView(
              slivers: [
                SliverList(
                    delegate: SliverChildListDelegate([
                  const CustomTitle(title: 'Categories'),
                  Container(
                    height: 50.h,
                    width: 100.w,
                    child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: listOfCategories.length,
                      itemBuilder: (context, index) => CategoryPill(
                          category: listOfCategories[index],
                          onPress: () {
                            setState(() {
                              listOfCategories.every((category) {
                                if (category.isSelected) {
                                  category.isSelected = false;
                                }
                                return true;
                              });
                              listOfCategories[index].isSelected = true;
                            });
                          }),
                    ),
                  ),
                  const CustomTitle(title: 'Last restaurants added')
                ])),
                SliverGrid.count(
                  crossAxisCount: 2,
                  crossAxisSpacing: 16.w,
                  childAspectRatio: .75,
                  mainAxisSpacing: 10.h,
                  children: data.data!
                      .map((item) => RestaurantCard(restaurant: item))
                      .toList(),
                )
              ],
            )),
        error: (error, s) => Center(child: Text('Error: $error')),
        loading: () => const CustomLoading());
  }
}
