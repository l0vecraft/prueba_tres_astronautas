import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_tres_astronautas/core/models/data_model.dart';
import 'package:prueba_tres_astronautas/ui/screens/detail/detail_screen.dart';
import 'package:prueba_tres_astronautas/ui/styles/app_style.dart';

class RestaurantCard extends StatelessWidget {
  final DataModel restaurant;
  const RestaurantCard({
    Key? key,
    required this.restaurant,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OpenContainer(
        closedElevation: 4,
        closedBuilder: (context, action) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 180.h,
                  decoration: BoxDecoration(
                      borderRadius: Appstyle.cardBorderRadius1,
                      image: DecorationImage(
                          image:
                              NetworkImage(restaurant.images!.original!.url!),
                          fit: BoxFit.fill)),
                ),
                Expanded(
                    child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 3.h),
                  child: Text(
                    restaurant.username!,
                    overflow: TextOverflow.ellipsis,
                    style:
                        TextStyle(fontSize: 14.sp, fontWeight: FontWeight.bold),
                  ),
                )),
                Expanded(
                    child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8.w),
                  child: Text(
                    restaurant.title!,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontSize: 14.sp,
                        color: Colors.grey.shade400,
                        fontWeight: FontWeight.bold),
                  ),
                )),
              ],
            ),
        openBuilder: (context, action) => DetailScreen(restaurant: restaurant));
  }
}
