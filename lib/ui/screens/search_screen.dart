import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_tres_astronautas/core/providers/restaurant_provider.dart';
import 'package:prueba_tres_astronautas/core/providers/search/search_provider.dart';
import 'package:prueba_tres_astronautas/ui/screens/favorite/widget/favorite_card.dart';
import 'package:prueba_tres_astronautas/ui/styles/app_style.dart';
import 'package:prueba_tres_astronautas/ui/widget/custom_loading.dart';

class SearchScreen extends ConsumerStatefulWidget {
  const SearchScreen({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _SearchScreenState();
}

class _SearchScreenState extends ConsumerState<SearchScreen> {
  var _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var searchData = ref.watch(searchProvider.notifier);
    var searchResult = ref.watch(searchFuture);
    return GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: NestedScrollView(
          headerSliverBuilder: (context, innerBoxIsScrolled) => [
            SliverAppBar(
              expandedHeight: 25.h,
              backgroundColor: Colors.white,
              flexibleSpace: Container(
                padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 5.h),
                child: TextField(
                  controller: _controller,
                  cursorColor: Colors.orange,
                  onSubmitted: (value) async {
                    setState(() => _controller.text = value);
                    if (_controller.text.isNotEmpty) {
                      searchData.searchTerm = value;

                      ref.read(searchProvider.notifier).state = await ref
                          .read(searchProvider.notifier)
                          .searchRestaurant();
                    }
                  },
                  decoration: InputDecoration(
                      hintText: 'Search',
                      focusedBorder: Appstyle.inputBorderRadius,
                      border: Appstyle.inputBorderRadius),
                ),
              ),
            )
          ],
          body: searchResult.when(
              data: (result) => result.data == null
                  ? const Center(
                      child: Text(
                          'No se ha encontrado nada, pruebe buscando algo.'),
                    )
                  : ListView.builder(
                      itemCount: result.pagination?.count,
                      itemBuilder: (context, index) =>
                          FavoriteCard(restaurant: result.data?[index])),
              error: (error, s) => Center(
                    child: Text('Error: $error'),
                  ),
              loading: () => const CustomLoading()),
        ));
  }
}
