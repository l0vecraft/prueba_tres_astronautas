import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:line_icons/line_icons.dart';
import 'package:prueba_tres_astronautas/core/models/category_model.dart';
import 'package:prueba_tres_astronautas/core/providers/restaurant_provider.dart';
import 'package:prueba_tres_astronautas/ui/screens/favorite/widget/favorite_card.dart';
import 'package:prueba_tres_astronautas/ui/styles/app_style.dart';
import 'package:prueba_tres_astronautas/ui/widget/category_pill.dart';
import 'package:prueba_tres_astronautas/ui/widget/circular_icon_buttom.dart';

class FavoriteScreen extends ConsumerStatefulWidget {
  const FavoriteScreen({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends ConsumerState<FavoriteScreen> {
  @override
  Widget build(BuildContext context) {
    var restaurantData = ref.read(restaurantProvider).listOfRestaurants;
    return Container(
      color: Colors.grey.shade100,
      child: CustomScrollView(
        slivers: [
          SliverList(
              delegate: SliverChildListDelegate([
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 5.h),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Favorites', style: Appstyle.title1),
                  CircularIconButton(
                    icon: LineIcons.plus,
                    onTap: () {},
                    outsideVerticalPadding: 0,
                    insideHorizontalPadding: 7.sp,
                    insideVerticalPadding: 7.sp,
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 2.h, horizontal: 8.w),
              child: Container(
                height: 50.h,
                width: 100.w,
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: listOfCategories.length,
                  itemBuilder: (context, index) => CategoryPill(
                      category: listOfCategories[index],
                      onPress: () {
                        setState(() {
                          listOfCategories.every((category) {
                            if (category.isSelected) {
                              category.isSelected = false;
                            }
                            return true;
                          });
                          listOfCategories[index].isSelected = true;
                        });
                      }),
                ),
              ),
            ),
          ])),
          SliverList(
              delegate: SliverChildListDelegate(restaurantData.data!
                  .map((restaurant) => Stack(
                        alignment: Alignment.bottomLeft,
                        children: [
                          FavoriteCard(restaurant: restaurant),
                          Positioned(
                            left: 68.w,
                            child: CircularIconButton(
                              icon: Icons.favorite_rounded,
                              color: Colors.orange,
                              onTap: () {},
                              insideVerticalPadding: 7.h,
                            ),
                          )
                        ],
                      ))
                  .toList()))
        ],
      ),
    );
  }
}
