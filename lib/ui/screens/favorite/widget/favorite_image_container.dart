import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_tres_astronautas/ui/styles/app_style.dart';

class FavoriteImageContainer extends StatelessWidget {
  const FavoriteImageContainer({
    Key? key,
    required this.image,
  }) : super(key: key);

  final String? image;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 15.h, horizontal: 8.w),
      child: Container(
        height: 100.h,
        width: 120.w,
        decoration: BoxDecoration(
            color: Colors.grey.shade200,
            borderRadius: Appstyle.cardBorderRadius1,
            image:
                DecorationImage(image: NetworkImage(image!), fit: BoxFit.fill)),
      ),
    );
  }
}
