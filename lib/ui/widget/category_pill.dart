import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_tres_astronautas/core/models/category_model.dart';

class CategoryPill extends StatelessWidget {
  final CategoryModel category;
  final VoidCallback onPress;
  const CategoryPill({
    Key? key,
    required this.category,
    required this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 6.w),
      child: InkWell(
          onTap: onPress,
          child: Chip(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            shadowColor: Colors.grey.shade200,
            elevation: 3,
            backgroundColor: category.isSelected ? Colors.orange : Colors.white,
            label: Text(
              category.name,
              style: TextStyle(
                  fontWeight:
                      category.isSelected ? FontWeight.w600 : FontWeight.w400,
                  fontSize: 18.sp,
                  color: category.isSelected ? Colors.white : Colors.black),
            ),
          )),
    );
  }
}
