import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CircularIconButton extends StatelessWidget {
  final IconData icon;
  final Color color;
  final VoidCallback onTap;
  final double outsideVerticalPadding;
  final double insideHorizontalPadding;
  final double insideVerticalPadding;
  const CircularIconButton(
      {Key? key,
      required this.icon,
      required this.onTap,
      this.outsideVerticalPadding = 6,
      this.insideHorizontalPadding = 8,
      this.insideVerticalPadding = 0,
      this.color = Colors.black})
      : super(key: key);

  @override
  Widget build(BuildContext context) => InkWell(
        onTap: onTap,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: outsideVerticalPadding),
          child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadiusDirectional.circular(50)),
            elevation: 10,
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: insideHorizontalPadding,
                  vertical: insideVerticalPadding),
              child: Icon(
                icon,
                color: color,
                size: 20.sp,
              ),
            ),
          ),
        ),
      );
}
