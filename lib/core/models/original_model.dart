import 'package:json_annotation/json_annotation.dart';

part 'original_model.g.dart';

@JsonSerializable()
class OriginalModel {
  @JsonKey(name: 'height')
  String? height;
  @JsonKey(name: 'width')
  String? width;
  @JsonKey(name: 'size')
  String? size;
  @JsonKey(name: 'url')
  String? url;
  @JsonKey(name: 'mp4_size')
  String? mp4Size;
  @JsonKey(name: 'mp4')
  String? mp4;
  @JsonKey(name: 'webp_size')
  String? webpSize;
  @JsonKey(name: 'webp')
  String? webp;
  @JsonKey(name: 'frames')
  String? frames;
  @JsonKey(name: 'hash')
  String? hash;

  OriginalModel(
      {this.height,
      this.width,
      this.size,
      this.url,
      this.mp4Size,
      this.mp4,
      this.webpSize,
      this.webp,
      this.frames,
      this.hash});

  factory OriginalModel.fromJson(Map<String, dynamic> json) =>
      _$OriginalModelFromJson(json);

  Map<String, dynamic> toJson() => _$OriginalModelToJson(this);
}
