import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable()
class UserModel {
  @JsonKey(name: 'avatar_url')
  String? avatarUrl;
  @JsonKey(name: 'banner_image')
  String? bannerImage;
  @JsonKey(name: 'banner_url')
  String? bannerUrl;
  @JsonKey(name: 'profile_url')
  String? profileUrl;
  @JsonKey(name: 'username')
  String? username;
  @JsonKey(name: 'display_name')
  String? displayName;
  @JsonKey(name: 'description')
  String? description;
  @JsonKey(name: 'instagram_url')
  String? instagramUrl;
  @JsonKey(name: 'website_url')
  String? websiteUrl;
  @JsonKey(name: 'is_verified')
  bool? isVerified;

  UserModel(
      {this.avatarUrl,
      this.bannerImage,
      this.bannerUrl,
      this.profileUrl,
      this.username,
      this.displayName,
      this.description,
      this.instagramUrl,
      this.websiteUrl,
      this.isVerified});

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}
