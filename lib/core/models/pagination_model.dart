import 'package:json_annotation/json_annotation.dart';

part 'pagination_model.g.dart';

@JsonSerializable()
class PaginationModel {
  @JsonKey(name: 'total_count')
  int? totalCount;
  @JsonKey(name: 'count')
  int? count;
  @JsonKey(name: 'offset')
  int? offset;

  PaginationModel({this.totalCount, this.count, this.offset});

  factory PaginationModel.fromJson(Map<String, dynamic> json) =>
      _$PaginationModelFromJson(json);

  Map<String, dynamic> toJson() => _$PaginationModelToJson(this);
}
