import 'package:json_annotation/json_annotation.dart';

import 'original_model.dart';

part 'images_model.g.dart';

@JsonSerializable()
class ImagesModel {
  @JsonKey(name: 'original')
  OriginalModel? original;

  ImagesModel({
    this.original,
  });

  factory ImagesModel.fromJson(Map<String, dynamic> json) =>
      _$ImagesModelFromJson(json);

  Map<String, dynamic> toJson() => _$ImagesModelToJson(this);
}
