import 'package:json_annotation/json_annotation.dart';

part 'meta_model.g.dart';

@JsonSerializable()
class MetaModel {
  @JsonKey(name: 'status')
  int? status;
  @JsonKey(name: 'msg')
  String? msg;
  @JsonKey(name: 'response_id')
  String? responseId;

  MetaModel({this.status, this.msg, this.responseId});

  factory MetaModel.fromJson(Map<String, dynamic> json) =>
      _$MetaModelFromJson(json);
  Map<String, dynamic> toJson() => _$MetaModelToJson(this);
}
