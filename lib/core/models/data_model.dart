import 'package:json_annotation/json_annotation.dart';
import 'package:prueba_tres_astronautas/core/models/result_model.dart';

import 'images_model.dart';
import 'user_model.dart';

part 'data_model.g.dart';

@JsonSerializable()
class DataModel {
  @JsonKey(name: 'type')
  String? type;
  @JsonKey(name: 'id')
  String? id;
  @JsonKey(name: 'url')
  String? url;
  @JsonKey(name: 'slug')
  String? slug;
  @JsonKey(name: 'bitly_gif_url')
  String? bitlyGifUrl;
  @JsonKey(name: 'bitly_url')
  String? bitlyUrl;
  @JsonKey(name: 'embed_url')
  String? embedUrl;
  @JsonKey(name: 'username')
  String? username;
  @JsonKey(name: 'source')
  String? source;
  @JsonKey(name: 'title')
  String? title;
  @JsonKey(name: 'rating')
  String? rating;
  @JsonKey(name: 'content_url')
  String? contentUrl;
  @JsonKey(name: 'source_tld')
  String? sourceTld;
  @JsonKey(name: 'source_post_url')
  String? sourcePostUrl;
  @JsonKey(name: 'is_sticker')
  int? isSticker;
  @JsonKey(name: 'import_date_time')
  String? importDatetime;
  @JsonKey(name: 'trending_date_time')
  String? trendingDatetime;
  @JsonKey(name: 'images')
  ImagesModel? images;
  @JsonKey(name: 'user')
  UserModel? user;
  @JsonKey(name: 'analytics_response_payload')
  String? analyticsResponsePayload;

  DataModel({
    this.type,
    this.id,
    this.url,
    this.slug,
    this.bitlyGifUrl,
    this.bitlyUrl,
    this.embedUrl,
    this.username,
    this.source,
    this.title,
    this.rating,
    this.contentUrl,
    this.sourceTld,
    this.sourcePostUrl,
    this.isSticker,
    this.importDatetime,
    this.trendingDatetime,
    this.images,
    this.user,
    this.analyticsResponsePayload,
  });

  factory DataModel.fromJson(Map<String, dynamic> json) =>
      _$DataModelFromJson(json);

  Map<String, dynamic> toJson() => _$DataModelToJson(this);
}
