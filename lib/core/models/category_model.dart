class CategoryModel {
  final String name;
  bool isSelected;

  CategoryModel({required this.name, this.isSelected = false});
}

final listOfCategories = [
  CategoryModel(name: 'pizza'),
  CategoryModel(name: 'pasta'),
  CategoryModel(name: 'hamburgers'),
  CategoryModel(name: 'ice cream'),
  CategoryModel(name: 'drinks'),
  CategoryModel(name: 'beer'),
  CategoryModel(name: 'coctels'),
];
