import 'package:json_annotation/json_annotation.dart';
import 'package:prueba_tres_astronautas/core/models/data_model.dart';

import 'meta_model.dart';
import 'pagination_model.dart';

part 'result_model.g.dart';

@JsonSerializable()
class ResultModel {
  @JsonKey(name: 'data')
  List<DataModel>? data;
  @JsonKey(name: 'pagination')
  PaginationModel? pagination;
  @JsonKey(name: 'meta')
  MetaModel? meta;

  ResultModel({this.data, this.pagination, this.meta});

  factory ResultModel.fromJson(Map<String, dynamic> json) =>
      _$ResultModelFromJson(json);

  Map<String, dynamic> toJson() => _$ResultModelToJson(this);
}
