// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'original_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OriginalModel _$OriginalModelFromJson(Map<String, dynamic> json) =>
    OriginalModel(
      height: json['height'] as String?,
      width: json['width'] as String?,
      size: json['size'] as String?,
      url: json['url'] as String?,
      mp4Size: json['mp4_size'] as String?,
      mp4: json['mp4'] as String?,
      webpSize: json['webp_size'] as String?,
      webp: json['webp'] as String?,
      frames: json['frames'] as String?,
      hash: json['hash'] as String?,
    );

Map<String, dynamic> _$OriginalModelToJson(OriginalModel instance) =>
    <String, dynamic>{
      'height': instance.height,
      'width': instance.width,
      'size': instance.size,
      'url': instance.url,
      'mp4_size': instance.mp4Size,
      'mp4': instance.mp4,
      'webp_size': instance.webpSize,
      'webp': instance.webp,
      'frames': instance.frames,
      'hash': instance.hash,
    };
