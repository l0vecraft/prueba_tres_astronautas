// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'images_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImagesModel _$ImagesModelFromJson(Map<String, dynamic> json) => ImagesModel(
      original: json['original'] == null
          ? null
          : OriginalModel.fromJson(json['original'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ImagesModelToJson(ImagesModel instance) =>
    <String, dynamic>{
      'original': instance.original,
    };
