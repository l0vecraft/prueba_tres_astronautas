import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:prueba_tres_astronautas/core/api/repository/restaurant_repository.dart';
import 'package:prueba_tres_astronautas/core/const/const.dart';
import 'package:prueba_tres_astronautas/core/exceptions/restaurant_exception.dart';
import 'package:prueba_tres_astronautas/core/models/result_model.dart';

class RestaurantApi implements RestaurantRepository {
  final Dio dio;
  RestaurantApi({required this.dio});

  final _baseUrl = 'https://api.giphy.com/v1/gifs/search?api_key';

  @override
  Future<ResultModel> getMainData() async {
    try {
      var url =
          '$_baseUrl=$s_key&q=restaurants&limit=10&offset=0&rating=g&lang=en';
      var response = await dio.get(url);
      return ResultModel.fromJson(response.data);
    } catch (e) {
      log(e.toString());
      throw RestaurantException(messaage: 'Error al obtener los restaurantes');
    }
  }

  @override
  Future<ResultModel> searchRestaurant(String value) async {
    try {
      var url = '$_baseUrl=$s_key&q=$value&limit=10&offset=0&rating=g&lang=en';
      var response = await dio.get(url);
      return ResultModel.fromJson(response.data);
    } catch (e) {
      log(e.toString());
      throw RestaurantException(messaage: 'Error al obtener los restaurantes');
    }
  }
}
