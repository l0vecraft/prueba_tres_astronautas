import 'package:prueba_tres_astronautas/core/models/result_model.dart';

abstract class RestaurantRepository {
  Future<ResultModel> getMainData();
  Future<ResultModel> searchRestaurant(String value);
}
