part of 'search_provider.dart';

abstract class SearchState extends Equatable {
  const SearchState();

  @override
  List<Object> get props => [];
}

class SearchInitial extends SearchState {}

class SearchLoading extends SearchState {}

class SearchLoaded extends SearchState {
  final ResultModel listOfResults;

  const SearchLoaded({required this.listOfResults});

  @override
  List<Object> get props => [listOfResults];
}

class SearchError extends SearchState {
  final String message;

  const SearchError({required this.message});
}
