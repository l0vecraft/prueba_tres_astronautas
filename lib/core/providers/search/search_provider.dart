import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:equatable/equatable.dart';
import 'package:prueba_tres_astronautas/core/api/repository/restaurant_repository.dart';
import 'package:prueba_tres_astronautas/core/exceptions/restaurant_exception.dart';
import 'package:prueba_tres_astronautas/core/models/result_model.dart';
import 'package:prueba_tres_astronautas/locator.dart';
part 'search_state.dart';

class SearchProvider extends StateNotifier<ResultModel> {
  final _api = l.get<RestaurantRepository>();

  var _searchTerm = '';
  var _listOfResults = ResultModel();

  SearchProvider() : super(ResultModel());

  String get searchTerm => _searchTerm;
  ResultModel get listOfResults => _listOfResults;

  set searchTerm(String value) => _searchTerm = value;

  Future<ResultModel> searchRestaurant() async {
    try {
      var result = await _api.searchRestaurant(_searchTerm);
      _listOfResults = result;
      return result;
    } catch (e) {
      rethrow;
    }
  }
}

var searchProvider = StateNotifierProvider<SearchProvider, ResultModel>(
    (ref) => SearchProvider());

var searchFuture =
    FutureProvider((ref) async => await ref.watch(searchProvider));
