import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:prueba_tres_astronautas/core/api/repository/restaurant_repository.dart';
import 'package:prueba_tres_astronautas/core/models/result_model.dart';
import 'package:prueba_tres_astronautas/locator.dart';

class RestaurantProvider extends ChangeNotifier {
  final _api = l.get<RestaurantRepository>();
  var _listOfRestaurants = ResultModel();

  ResultModel get listOfRestaurants => _listOfRestaurants;

  Future<ResultModel> getRestaurants() async {
    try {
      var result = await _api.getMainData();
      _listOfRestaurants = result;
      return result;
    } catch (e) {
      rethrow;
    }
  }
}

var restaurantProvider =
    ChangeNotifierProvider<RestaurantProvider>((ref) => RestaurantProvider());

var restaurantInitData = FutureProvider(
    (ref) async => await ref.watch(restaurantProvider).getRestaurants());
